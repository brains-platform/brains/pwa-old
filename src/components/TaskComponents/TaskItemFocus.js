import React, { Component } from 'react'
import { MdArrowBack, MdDelete, MdClose } from 'react-icons/lib/md'
import { FaBars, FaCalendarO } from 'react-icons/lib/fa'
import AutoResizeTextArea from 'react-textarea-autosize'

import Modal from '../Modal'

class TaskItemFocus extends Component {
  constructor(props) {
    super(props)

    this.handleChangeTitleText = this.handleChangeTitleText.bind(this)
    this.handleChangeDescriptionText = this.handleChangeDescriptionText.bind(
      this
    )
    this.handleGoBack = this.handleGoBack.bind(this)
    this.handleCloseError = this.handleCloseError.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleCancelDelete = this.handleCancelDelete.bind(this)
    this.handleOpenConfirmDelete = this.handleOpenConfirmDelete.bind(this)
    this.getInsideModalRender = this.getInsideModalRender.bind(this)
    this.handleDeleteDeadline = this.handleDeleteDeadline.bind(this)

    this.state = {
      title: this.props.task.title,
      description: this.props.task.description,
      emptyTitleError: false,
      confirmDeleteModal: false,
    }
  }

  handleGoBack() {
    if (this.state.title.length === 0) {
      this.setState({ emptyTitleError: true })
    } else {
      this.props.task.title = this.state.title
      this.props.task.description = this.state.description
      this.props.handleChangeAllTaskFields()
      this.props.handleUnfocusTask()
    }
  }

  handleCloseError() {
    this.setState({ emptyTitleError: false })
  }

  handleChangeTitleText(ev) {
    this.setState({ title: ev.target.value })
  }

  handleChangeDescriptionText(ev) {
    this.setState({ description: ev.target.value })
  }

  handleDelete() {
    this.setState({ confirmDeleteModal: false })
    this.props.handleUnfocusTask()
    this.props.handleDelete()
  }

  handleCancelDelete() {
    this.setState({ confirmDeleteModal: false })
  }

  handleOpenConfirmDelete() {
    this.setState({ confirmDeleteModal: true })
  }

  getError() {
    if (this.state.emptyTitleError) {
      return (
        <span className="task-focused-error">
          Digite um título para a tarefa!
          <MdClose onClick={this.handleCloseError} />
        </span>
      )
    }
  }

  getConfirmDeleteModal() {
    if (this.state.confirmDeleteModal) {
      return <Modal render={this.getInsideModalRender()} />
    }
  }

  getInsideModalRender() {
    return (
      <div className="task-focused-delete-modal-container">
        Deseja mesmo deletar esta tarefa?
        <div className="task-focused-delete-modal-buttons-container">
          <div
            className="task-focused-delete-modal-button"
            onClick={this.handleDelete}
          >
            {' '}
            OK{' '}
          </div>
          <div
            className="task-focused-delete-modal-button"
            onClick={this.handleCancelDelete}
          >
            {' '}
            Cancelar{' '}
          </div>
        </div>
      </div>
    )
  }

  handleDeleteDeadline(ev) {
    ev.stopPropagation()
    if (this.props.editable) {
      this.props.handleChangeDate(null, true)
    }
  }

  getDateDiv() {
    const text = this.props.deadline ? this.props.deadline.ddmm() : 'Data'
    const dateClasses = ['task-focused-date-container']
    if (this.props.deadline) {
      dateClasses.push('task-focused-date-valid')
    }
    return (
      <div className="task-focused-date" onClick={this.props.handleOpenModal}>
        <FaCalendarO className="task-focused-icons" />
        <div className={dateClasses.join(' ')}>
          {text}
          {this.props.deadline ? (
            <MdClose
              className="task-focused-date-close-icon"
              onClick={this.handleDeleteDeadline}
            />
          ) : (
            ''
          )}
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="task-focused-container">
        {this.props.renderCalendarModal()}
        {this.getError()}
        {this.getConfirmDeleteModal()}

        <div className="task-focused-header">
          <MdArrowBack onClick={this.handleGoBack} />
          <MdDelete onClick={this.handleOpenConfirmDelete} />
        </div>

        <AutoResizeTextArea
          className="task-focused-textarea task-focused-title"
          minRows={1}
          maxRows={3}
          value={this.state.title}
          readOnly={!this.props.editable}
          placeholder={this.props.editable ? 'Adicione um Título' : ''}
          onChange={this.handleChangeTitleText}
        />

        <div className="task-focused-description-container">
          <FaBars className="task-focused-icons" />
          <AutoResizeTextArea
            className="task-focused-textarea"
            minRows={1}
            maxRows={3}
            value={this.state.description}
            readOnly={!this.props.editable}
            placeholder={this.props.editable ? 'Adicione detalhes...' : ''}
            onChange={this.handleChangeDescriptionText}
            style={{ alignSelf: 'stretch' }}
          />
        </div>

        {this.getDateDiv()}
      </div>
    )
  }
}

export default TaskItemFocus
