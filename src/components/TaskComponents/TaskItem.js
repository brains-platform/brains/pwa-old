import React, { Component } from 'react'
import { MdArchive, MdUnarchive } from 'react-icons/lib/md'
import {
  FaCalendarTimesO,
  FaCalendarO,
  FaCalendarCheckO,
} from 'react-icons/lib/fa'
import gql from 'graphql-tag'

import { client } from '../../index'

import TaskCalendarModal from './TaskCalendarModal'
import TaskItemFocus from './TaskItemFocus'

Date.prototype.ddmm = function() {
  let mm = this.getMonth()
  let dd = this.getDate()
  const dictMes = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Maio',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez',
  ]
  return `${dd} ${dictMes[mm]}`
}

function DateToDateTimeString(date) {
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
}

async function submitUpdate(update, variables) {
  console.log(variables)
  const response = await client.mutate({
    mutation: update,
    variables,
  })
  console.log('Update', response)
}

class TaskItem extends Component {
  constructor(props) {
    super(props)

    this.handleCheckChange = this.handleCheckChange.bind(this)
    this.handleArchive = this.handleArchive.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleChangeDate = this.handleChangeDate.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleChangeAllTaskFields = this.handleChangeAllTaskFields.bind(this)
    this.setRemainingTimeData = this.setRemainingTimeData.bind(this)
    this.getDeadlineDiv = this.getDeadlineDiv.bind(this)
    this.handleChangeDate = this.handleChangeDate.bind(this)
    this.getCalendarModal = this.getCalendarModal.bind(this)

    this.state = {
      isDone: this.props.task.done,
      calendarModalOpen: false,
    }

    this.remainingTimeData = null
    this.setRemainingTimeData(this.state.isDone)
  }

  handleCheckChange() {
    const changeDone = gql`
      mutation changeDone($taskId: ID!, $done: Boolean!) {
        updateTaskDone(id: $taskId, done: $done) {
          id
          done
        }
      }
    `

    this.setState(prevState => {
      this.setRemainingTimeData(!prevState.isDone)
      this.props.task.done = !prevState.isDone
      submitUpdate(changeDone, {
        taskId: this.props.task.id,
        done: this.props.task.done,
      })
      return { isDone: !prevState.isDone }
    })
  }

  handleOpenModal() {
    if (this.props.isArchived === false) {
      this.setState({ calendarModalOpen: true })
    }
  }

  handleArchive() {
    const changeArchived = gql`
      mutation changeArchived($taskId: ID!, $archived: Boolean!) {
        updateTaskArchived(id: $taskId, archived: $archived) {
          id
          archived
        }
      }
    `

    submitUpdate(changeArchived, {
      taskId: this.props.task.id,
      archived: !this.props.task.archived,
    })

    this.props.handleArchive(this.props.task)
  }

  handleDelete() {
    const deleteTask = gql`
      mutation deleteTask($taskId: ID!) {
        deleteTask(id: $taskId) {
          id
          title
          description
          deadline
          done
          archived
        }
      }
    `

    submitUpdate(deleteTask, {
      taskId: this.props.task.id,
    })

    this.props.handleDelete(this.props.task)
  }

  handleChangeDate(newDate, acceptNull) {
    if (newDate != null || acceptNull !== false) {
      const changeDeadline = gql`
        mutation changeDeadline($taskId: ID!, $deadline: DateTime) {
          updateTaskDeadline(id: $taskId, deadline: $deadline) {
            id
            deadline
          }
        }
      `
      this.props.task.deadline = newDate
      submitUpdate(changeDeadline, {
        taskId: this.props.task.id,
        deadline:
          this.props.task.deadline == null
            ? null
            : DateToDateTimeString(this.props.task.deadline),
      })
    }
    this.setState(prevState => {
      this.setRemainingTimeData(prevState.isDone)
      return { calendarModalOpen: false }
    })
  }

  handleChangeAllTaskFields() {
    const changeAll = gql`
      mutation changeAll(
        $taskId: ID!
        $title: String!
        $description: String!
        $deadline: DateTime
        $done: Boolean!
        $archived: Boolean!
      ) {
        updateTask(
          id: $taskId
          title: $title
          description: $description
          deadline: $deadline
          done: $done
          archived: $archived
        ) {
          id
          title
          description
          deadline
          done
          archived
        }
      }
    `

    submitUpdate(changeAll, {
      taskId: this.props.task.id,
      title: this.props.task.title,
      description: this.props.task.description,
      deadline:
        this.props.task.deadline == null
          ? null
          : DateToDateTimeString(this.props.task.deadline),
      done: this.props.task.done,
      archived: this.props.task.archived,
    })
  }

  setRemainingTimeData(isDone) {
    if (this.props.task.deadline) {
      this.remainingTimeData = {}
      let today = new Date()
      const daysInMs = 1000 * 3600 * 24
      this.remainingTimeData.remaining = Math.ceil(
        (this.props.task.deadline.getTime() - today.getTime()) / daysInMs
      )
      if (this.remainingTimeData.remaining >= 0 || isDone === true) {
        this.remainingTimeData.style = { color: 'blue' }
      } else {
        this.remainingTimeData.style = { color: 'red' }
      }
      this.remainingTimeData.string =
        this.remainingTimeData.remaining >= 0 || isDone === true
          ? this.props.task.deadline.ddmm()
          : Math.abs(this.remainingTimeData.remaining) + ' dias atrasado'
    }
  }

  getCalendarModal() {
    if (this.state.calendarModalOpen) {
      return (
        <TaskCalendarModal
          deadline={this.props.task.deadline}
          closeModal={obj => this.handleChangeDate(obj, false)}
        />
      )
    }
  }

  getArchiveOption() {
    if (this.props.isArchived) {
      return (
        <MdUnarchive
          className="task-archive-option"
          onClick={this.handleArchive}
        />
      )
    } else if (this.state.isDone) {
      return (
        <MdArchive
          className="task-archive-option"
          onClick={this.handleArchive}
        />
      )
    }
  }

  getDeadlineCalendarIcon() {
    if (this.state.isDone) {
      return <FaCalendarCheckO className="task-deadline-icon" />
    } else if (this.remainingTimeData.remaining >= 0) {
      return <FaCalendarO className="task-deadline-icon" />
    } else {
      return <FaCalendarTimesO className="task-deadline-icon" />
    }
  }

  getDeadlineDiv(classes) {
    if (this.props.task.deadline) {
      return (
        <div
          className={classes.join(' ')}
          style={this.remainingTimeData.style}
          onClick={this.handleOpenModal}
        >
          {this.getDeadlineCalendarIcon()}
          {this.remainingTimeData.string}
        </div>
      )
    }
  }

  getNormalRender() {
    const taskTitleClasses =
      'task-title' + (this.state.isDone === true ? ' task-title-done' : '')
    const focusMeFunction = ev => this.props.handleFocusTask(this.props.task)
    return (
      <div className="task-item">
        {this.getCalendarModal()}
        <input
          type="checkbox"
          className="task-checkbox"
          checked={this.state.isDone}
          disabled={this.props.isArchived}
          onChange={this.handleCheckChange}
        />
        <div className="task-content">
          <div className={taskTitleClasses} onClick={focusMeFunction}>
            {this.props.task.title}
          </div>
          {this.getDeadlineDiv(['task-deadline'])}
        </div>
        {this.getArchiveOption()}
      </div>
    )
  }

  getItemFocusRender() {
    return (
      <TaskItemFocus
        task={this.props.task}
        deadline={this.props.task.deadline}
        handleUnfocusTask={this.props.handleUnfocusTask}
        handleOpenModal={this.handleOpenModal}
        handleDelete={this.handleDelete}
        handleChangeDate={this.handleChangeDate}
        handleChangeAllTaskFields={this.handleChangeAllTaskFields}
        renderCalendarModal={this.getCalendarModal}
        editable={this.props.isArchived === false}
      />
    )
  }

  render() {
    if (this.props.isTaskFocused === true) {
      return this.getItemFocusRender()
    } else {
      return this.getNormalRender()
    }
  }
}

export default TaskItem
