import React, { Component } from 'react'

import AppRouter from '../routers/AppRouter'

import '../styles/main.css'

class App extends Component {
  render() {
    return (
      <div id="app">
        <AppRouter />
      </div>
    )
  }
}

export default App
