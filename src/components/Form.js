import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Form extends Component {
  constructor(props) {
    super(props)

    this.state = this.props.state

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.renderQuestion = this.renderQuestion.bind(this)
    this.renderDelete = this.renderDelete.bind(this)
  }

  handleChange(event) {
    this.setState({ [`${event.target.name}`]: event.target.value })
  }

  handleSubmit(event) {
    event.preventDefault()
    this.props.submitHandler(this.state)
  }

  renderFormInput(inputs) {
    return inputs.map(({ name, input }) => {
      return (
        <div className="form-item" key={name}>
          <p className="form-item-label">{name}</p>
          <input
            name={input.name}
            type={input.type}
            value={this.state[`${input.name}`]}
            onChange={this.handleChange}
          />
        </div>
      )
    })
  }

  renderQuestion() {
    if (this.props.link === undefined) return ''

    return (
      <p>
        {' '}
        {this.props.question}
        <Link to={this.props.link}> {this.props.answer} </Link>
      </p>
    )
  }

  renderDelete() {
    if (!this.props.deleteHandler) return ''

    return (
      <button
        onClick={() => this.props.deleteHandler()}
        className="btn btn-red"
      >
        Deletar
      </button>
    )
  }

  renderErrors(errors) {
    if (errors) {
      return errors.map(err => {
        return(
          <p key={err} className="error-message"> { err } </p>
        )
      })
    }
  }

  render() {
    return (
      <div className="form-container">
        <div className="form-header">{this.props.title}</div>

        <form className="form-content">
          { this.renderFormInput(this.props.inputs) }
          <div className="form-button-container">
            { this.renderErrors(this.props.errors) }
            <button
              className="btn btn-dark-mode"
              type="submit"
              onClick={this.handleSubmit}
            >
              {this.props.action}
            </button>
            {this.renderDelete()}
            {this.renderQuestion()}
          </div>
        </form>


      </div>
    )
  }
}

export default Form
