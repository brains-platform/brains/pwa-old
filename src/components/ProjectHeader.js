import React from 'react'
import { FaArrowLeft } from 'react-icons/lib/fa'

function createLink(callback) {
  if (callback !== undefined) {
    return <FaArrowLeft className="header-icon" onClick={callback} />
  }
  return ''
}

const MenuItem = props => {
  const classes =
    'project-menu-item' +
    (props.isSelected ? ' project-menu-item-selected' : '')
  return (
    <div
      className={classes}
      onClick={ev => props.item.callback(ev, props.item)}
    >
      {props.item.option}
    </div>
  )
}

const ProjectMenu = props => {
  const menuList = props.menu.map(item => (
    <MenuItem
      key={item.index}
      item={item}
      isSelected={item.index === props.menuOptionSelected}
    />
  ))
  return (
    <div id="project-menu" className="project-menu">
      {menuList}
    </div>
  )
}

const ProjectHeader = props => {
  return (
    <header className="project-header">
      <div className="project-header-top">
        {createLink(props.callbackGoBackPage)}

        <div className="project-header-title">{props.title}</div>
      </div>

      <ProjectMenu
        menu={props.menu}
        menuOptionSelected={props.menuOptionSelected}
      />
    </header>
  )
}

export default ProjectHeader
