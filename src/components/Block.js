import React, { Component } from 'react'
import { FaPencil } from 'react-icons/lib/fa'

class Block extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: props.title,
      subtitle: props.title,
    }
  }

  renderEditIcon() {
    if (this.props.edit) {
      return (
        <FaPencil className="block-icon" onClick={() => console.log("edit")} />
      )
    }
  }

  render() {
    let className = "block "
    if (this.props.gray === 'gray') className += "block-gray "
    return (
      <div className={className}> 
        <h3 className="block-title">{this.props.title}</h3>
        <p className="block-subtitle">{this.props.subtitle}</p>
      </div>
    )
  }
}

export default Block