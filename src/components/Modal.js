import React, { Component } from 'react'

class Modal extends Component {
  render() {
    return (
      <div className="backdrop">
        <div className="modal">{this.props.render}</div>
      </div>
    )
  }
}

export default Modal
