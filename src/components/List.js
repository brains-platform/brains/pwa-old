import React from 'react'

const List = props => {
  if (!props.objList)
    return null;

  return (
    <div className="list">
      {props.objList.map(props.itemRender)}
    </div>
  )
}

export default List
