import React, { Fragment, Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import ProjectView from '../containers/ProjectView'

import NotFoundPage from '../components/NotFoundPage'
import HomePage from '../components/HomePage'
import SchoolList from '../containers/SchoolList'
import ClassroomList from '../containers/ClassroomList'

import ProjectList from '../containers/ProjectList'
import ProjectListComposed from '../containers/ProjectListComposed'

import SchoolCreate from '../containers/SchoolCreate'
import ClassroomCreate from '../containers/ClassroomCreate'
import ProjectCreate from '../containers/ProjectCreate'

import SchoolEdit from '../containers/SchoolEdit'
import ClassroomEdit from '../containers/ClassroomEdit'

import Login from '../containers/Login'
import Register from '../containers/Register'

import Menu from '../components/Menu'
import MenuClass from '../containers/MenuClass'

import ProfileManagement from '../containers/ProfileManagement'
import ClassManagement from '../containers/ClassManagement'

import SideBarContent, { setStyle } from '../containers/SideBarContent'
import { toggleMenu, openMenu } from '../actions/openMenu.js'

import Sidebar from 'react-sidebar'
import Header from '../containers/Header'
import NewProjectView from '../containers/NewProjectView';
import NewSchoolList from '../containers/NewSchoolList';
import NewClassroomList from '../containers/NewClassroomList';
import NewProjectList from '../containers/NewProjectList';

const AppRouter = props => (
  <BrowserRouter>
    <Route render={ ({ location, history }) => (
      <Fragment>
        <Header pageHistory={history}/>
        <Sidebar
          sidebar={
            <SideBarContent
              redirect={to => {
                if (location.pathname !== to)
                  history.push(to)
              }}
            />
          }
          open={props.menu}
          onSetOpen={props.openMenu}
          pullRight={ true }
          styles={ setStyle(props.headerHeight) }
          overlayId='overlay'
        >
          <Switch>
            <Route path="/" component={HomePage} exact={true} />
            <Route path="/project_list" component={NewProjectList} exact={true}/>
            <Route path="/project_list_composed" component={ProjectListComposed} exact={true}/>
            <Route path="/school_list" component={NewSchoolList} />
            <Route path="/classroom_list" component={NewClassroomList} />

            <Route path="/project" component={ProjectView} />
            <Route path="/project_new" component={ProjectCreate} />
            <Route path="/school_new" component={SchoolCreate} />
            <Route path="/classroom_new" component={ClassroomCreate} />

            <Route path="/school_edit" component={SchoolEdit} />
            <Route path="/classroom_edit" component={ClassroomEdit} />

            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />

            <Route path="/profile_management" component={ProfileManagement} />
            <Route path="/class_management" component={ClassManagement} />

            <Route path="/menu" component={Menu} />
            <Route path="/class_menu" component={MenuClass} />

            <Route path="/new_project_view" component={NewProjectView} />
            <Route path="/old_school_list" component={SchoolList} />
            <Route path="/old_classroom_list" component={ClassroomList} />
            <Route path="/old_project_list" component={ProjectList} />

            <Route component={NotFoundPage} />
          </Switch>
        </Sidebar>
      </Fragment>
    )}
    />
  </BrowserRouter>
)

const mapDispatchToProps = dispatch => {
  const actions = {
    openMenu,
    toggleMenu
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    menu: state.menu,
    headerHeight: state.headerHeight
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppRouter)
