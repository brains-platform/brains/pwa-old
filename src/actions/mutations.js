import gql from 'graphql-tag';

export const createProject = gql`
  mutation createProjectFunc(
    $name: String!
    $objective: String
    $description: String
    $methodology: String
    $justification: String
    $marketing: String
    $product: String
    $result: String
  ) {
    createProject(
    name: $name
    objective: $objective
    description: $description
    methodology: $methodology
    justification: $justification
    marketing: $marketing
    product: $product
    result: $result
    )
    {
    id
    name
    }
  }
  `

export const connectStudents = gql`
  mutation connectStudentsFunc($id: ID!, $emails: [String!]!) {
    connectUsersToProjectAsTeam(id: $id, emails: $emails) {
    name
    }
  }
  `

export const connectAdvisors = gql`
  mutation connectAdvisorsFunc($id: ID!, $emails: [String!]!) {
    connectUsersToProjectAsAdvisors(id: $id, emails: $emails) {
    name
    }
  }
  `

export const updtProject = gql`
  mutation updateProjectFunc(
    $id: ID!
    $name: String
    $objective: String
    $description: String
    $methodology: String
    $justification: String
    $marketing: String
    $product: String
    $result: String
  ) {
    updateProject(
    id: $id
    name: $name
    objective: $objective
    description: $description
    methodology: $methodology
    justification: $justification
    marketing: $marketing
    product: $product
    result: $result
    )
    {
    id
    }
  }
  `

export const delProject = gql`
mutation deleteProjectFunc($id: ID!) {
  deleteProject(id: $id) {
    id
  }
}
`