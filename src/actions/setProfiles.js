export const SET_PROFILES = 'SET_PROFILES'
export const CLEAN_PROFILES = 'CLEAN_PROFILES'

export const setProfiles = profiles => {
  localStorage.setItem('profiles', profiles)

  return {
    type: SET_PROFILES,
    profiles
  }
}

export const cleanProfiles = () => ({
  type: CLEAN_PROFILES,
  payload: null
})
