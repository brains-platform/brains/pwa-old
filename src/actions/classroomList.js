import gql from 'graphql-tag'

import { client } from '..'

export const FETCH_CLASSROOMS = 'FETCH_CLASSROOMS'
export const SELECT_CLASSROOM = 'SELECT_CLASSROOM'
export const CLEAN_CLASSROOM = 'CLEAN_CLASSROOM'
export const ADD_CLASSROOM = 'ADD_CLASSROOM'
export const UPDATE_CLASSROOM = 'UPDATE_CLASSROOM'
export const DELETE_CLASSROOM = 'DELETE_CLASSROOM'

export const fetchClassrooms = id => async dispatch => {
  const getClassrooms = gql`
    query getClassroomBySchool($id: ID!) {
      getSchool(id: $id) {
        classrooms {
          id
          year
          complement
          period
        }
      }
    }
  `

  const response = await client.query({
    query: getClassrooms,
    variables: {
      id,
    },
  })

  dispatch({
    type: FETCH_CLASSROOMS,
    classroomList: response.data.getSchool.classrooms,
  })
}

export const selectClassroom = classroom => {
  localStorage.setItem('classroom', JSON.stringify(classroom))
  return {
    type: SELECT_CLASSROOM,
    classroom,
  }
}

export const cleanClass = () => ({
  type: CLEAN_CLASSROOM,
  payload: null
})

export const newClassroom =
  ({ year, complement, period }, schoolId) => async dispatch => {
  const createClassroom = gql`
    mutation createClassroomFunc(
      $year: String!
      $complement: String!
      $period: String!
      $schoolId: ID!
    ) {
      createClassroom(
        year: $year
        complement: $complement
        period: $period
        schoolId: $schoolId
      ) {
        id
        year
        complement
        period
      }
    }
  `

  const response = await client.mutate({
    mutation: createClassroom,
    variables: {
      year,
      complement,
      period,
      schoolId
    }
  })

  dispatch({
    type: ADD_CLASSROOM,
    payload: response.data.createClassroom
  })
}

export const updateClassroom =
  (id, { year, complement, period }) => async dispatch => {
  const updateClassroom = gql`
    mutation updateClassroomFunc(
      $id: ID!
      $year: String!
      $complement: String!
      $period: String!
    ) {
      updateClassroom(
        id: $id
        year: $year
        complement: $complement
        period: $period
      ) {
        id
        year
        complement
        period
      }
    }
  `

  const response = await client.mutate({
    mutation: updateClassroom,
    variables: {
      id,
      year,
      complement,
      period
    }
  })

  dispatch({
    type: UPDATE_CLASSROOM,
    payload: response.data.updateClassroom
  })

  dispatch(selectClassroom(response.data.updateClassroom))
}

export const deleteClassroom = id => async dispatch => {
  const deleteClassroom = gql`
    mutation deleteClassroomFunc($id: ID!) {
      deleteClassroom(id: $id) {
        id
      }
    }
  `

  const response = await client.mutate({
    mutation: deleteClassroom,
    variables: {
      id
    }
  })

  dispatch({
    type: DELETE_CLASSROOM,
    payload: response.data.deleteClassroom
  })

  dispatch(cleanClass())
}
