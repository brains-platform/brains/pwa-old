import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import gql from 'graphql-tag'

import { client } from '..'

import ProjectHeader from '../components/ProjectHeader'
import ProjectInfoCard from '../components/ProjectInfoCard'
import ProgressBar from '../components/ProgressBar'
import TeamList from '../components/TeamComponents/TeamList'
import ToDoPanel from './ToDoPanel'
import { setHeader } from '../actions/setHeader'

function isInside(obj, arr) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === obj) return true
  }
  return false
}

function taskDeadlineValidation(task) {
  if (task.deadline == null) return { ...task }
  return { ...task, deadline: new Date(task.deadline.slice(0, -1)) }
}

class ProjectView extends Component {
  constructor(props) {
    super(props)

    this.noCardRender = ['id', 'name', '__typename']

    this.team = null
    this.tasks = null
    this.projectInfo = null

    this.state = {
      infoReady: false,
      tasksReady: false,
      teamReady: false,
      menuOptionSelected: 0,
    }

    const menuOptions = ['EQUIPE', 'RELATÓRIO', 'DIÁRIO DE BORDO', 'MURAL']
    this.menu = menuOptions.map((option, index) => {
      return {
        option: option,
        index: index,
        callback: (ev, obj) => {
          this.setState({ menuOptionSelected: obj.index })
        },
      }
    })

    this.getProjectData(this.props.activeProject.id)
    this.getTasks(this.props.activeProject.id)
    this.getTeam(this.props.activeProject.id)
  }

  async getProjectData() {
    const getProjectInfo = gql`
      query getProjectInfo($projectId: ID!) {
        getProject(id: $projectId) {
          id
          name
          description
          justification
          methodology
          marketing
          product
          result
          objective
        }
      }
    `
    const response = await client.query({
      query: getProjectInfo,
      variables: {
        projectId: this.props.activeProject.id,
      },
    })

    console.log('Get projectInfo', response)
    this.projectInfo = { ...response.data.getProject }
    this.setState({
      infoReady: true,
    })
  }

  async getTasks() {
    const getProjectTasks = gql`
      query getProjectInfo($projectId: ID!) {
        getProject(id: $projectId) {
          id
          tasks {
            id
            title
            description
            deadline
            done
            archived
          }
        }
      }
    `
    const response = await client.query({
      query: getProjectTasks,
      variables: {
        projectId: this.props.activeProject.id,
      },
    })

    this.tasks = response.data.getProject.tasks.map(taskDeadlineValidation)
    this.setState({
      tasksReady: true,
    })
  }

  async getTeam() {
    const getProjectTeam = gql`
      query getProjectInfo($projectId: ID!) {
        getProject(id: $projectId) {
          id
          students {
            id
            email
            name
          }
        }
      }
    `
    const response = await client.query({
      query: getProjectTeam,
      variables: {
        projectId: this.props.activeProject.id,
      },
    })

    this.team = response.data.getProject.students.map(member => {
      return { ...member }
    })
    this.setState({
      teamReady: true,
    })
  }

  getCardList() {
    let cardList = []
    Object.keys(this.projectInfo).forEach(key => {
      if (isInside(key, this.noCardRender) === false)
        cardList.push({ name: key, value: this.projectInfo[key] })
    })
    return cardList
  }

  getRenderOption() {
    switch (this.state.menuOptionSelected) {
      case 0:
        if (!this.state.teamReady) return ''
        return <TeamList members={this.team} />
      case 1:
        return this.getCardList().map(item => (
          <ProjectInfoCard
            key={item.name}
            projectId={this.projectInfo.id}
            title={item.name}
            text={item.value}
          />
        ))
      case 2:
        if (!this.state.tasksReady) return ''
        return <ToDoPanel projectId={this.projectInfo.id} tasks={this.tasks} />
      case 3:
        return <div>Mural</div>
      default:
        return 'Error'
    }
  }

  render() {
    if (!this.state.infoReady) return <div> Loading </div>
    return (
      <section className="page">
        <ProjectHeader
          callbackGoBackPage={() => this.props.history.goBack()}
          title={this.projectInfo.name}
          menu={this.menu}
          menuOptionSelected={this.state.menuOptionSelected}
        />
        <ProgressBar progress="20" />
        {this.getRenderOption()}
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {}

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    activeProject: state.activeProject,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectView)
