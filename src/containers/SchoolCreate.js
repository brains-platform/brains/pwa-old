import React, { Component } from 'react'
import gql from 'graphql-tag'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { client } from '..'
import { setHeader } from '../actions/setHeader'
import { newSchool } from '../actions/schoolList'
import Form from '../components/Form'

class SchoolCreate extends Component {
  constructor(props) {
    super(props)

    this.submit = this.submit.bind(this)
  }

  componentDidMount() {
    this.props.setHeader(
      'Brains',
      () => this.props.history.goBack()
    )
  }

  async submit(args) {
    this.props.newSchool(args)
    this.props.history.push('/school_list')
  }

  render() {
    const inputs = [
      {
        name: 'Nome',
        input: {
          name: 'name',
          type: 'text',
        },
      }
    ]

    const state = {
      name: '',
    }

    return (
      <section className="form-section">

        <Form
          title="Criar Escola"
          state={state}
          inputs={inputs}
          submitHandler={this.submit}
          action="Criar"
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    newSchool,
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

export default connect(null, mapDispatchToProps)(SchoolCreate)
