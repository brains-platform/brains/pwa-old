import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import List from '../components/List'
import { fetchMultipleProjects, selectProject } from '../actions/projectList'
import { setHeader } from '../actions/setHeader'

class ProjectList extends Component {
  constructor(props) {
    super(props)

    this.itemRender = this.itemRender.bind(this)
    this.projectClick = this.projectClick.bind(this)
  }

  componentDidMount() {
    this.props.setHeader(
      'Projetos',
      () => this.props.history.goBack(),
      () => this.props.history.push('/project_new')
    )
    this.props.fetchMultipleProjects(this.props.activeProfiles)
  }

  projectClick(obj) {
    this.props.history.push('/project')
    this.props.selectProject(obj)
  }

  itemRender(obj) {
    return (
      <div key={obj.id} onClick={e => this.projectClick(obj)} className="card">
        <div className="card-title"> {obj.name} </div>
      </div>
    )
  }

  render() {
    return (
      <section className="page">
        <List
          objList={this.props.projectListStudent}
          itemRender={this.itemRender}
          buttonLink={'projects_new'}
        />

        <List
          objList={this.props.projectListAdvisor}
          itemRender={this.itemRender}
          buttonLink={'projects_new'}
        />

        <List
          objList={this.props.projectListResponsible}
          itemRender={this.itemRender}
          buttonLink={'projects_new'}
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader,
    fetchMultipleProjects,
    selectProject,
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    activeClass: state.activeClass,
    activeProfiles: state.activeProfiles,
    projectListStudent: state.projectListStudent,
    projectListAdvisor: state.projectListAdvisor,
    projectListResponsible: state.projectListResponsible

  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectList)
