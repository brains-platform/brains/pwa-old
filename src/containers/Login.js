import React, { Component } from 'react'
import gql from 'graphql-tag'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { client } from '..'
import { setHeader, setHeaderLandingLoginPage } from '../actions/setHeader'
import Form from '../components/Form'

import { setProfiles } from '../actions/setProfiles'

class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      errors: []
    }

    this.submit = this.submit.bind(this)
  }

  componentDidMount() {
    this.props.setHeader(
      'Brains',
      () => this.props.history.goBack()
    )
  }

  async submit(args) {
    console.log('hello')

    const login = gql`
      query loginFunc($email: String!, $password: String!) {
        login(email: $email, password: $password) {
          token,
          profiles
        }
      }
    `

    const response = await client.query({
      query: login,
      variables: {
        email: args.email,
        password: args.password,
      },
      errorPolicy: 'all',
    })

    const loginSuccess = (url) => {
      this.props.setHeaderLandingLoginPage(url)
      this.props.history.push(url);
    }

    if (response.data){
      localStorage.setItem('token', response.data.login.token)

      const profiles = response.data.login.profiles
      this.props.setProfiles(profiles)

      if (profiles.includes('MASTER')) 
        loginSuccess('/menu')
      else if (profiles.includes('TEACHER'))
        loginSuccess('/school_list')
      else if (profiles.length >= 2)
        loginSuccess('/project_list_composed')
      else
        loginSuccess('/project_list')
    } else {
      let errors = []
      response.errors.map(error => {
        console.log(error.message)
        errors.push(error.message)
      })
      this.setState({ errors })
    }
  }

  render() {
    const inputs = [
      {
        name: 'Email',
        input: {
          name: 'email',
          type: 'email',
        },
      },
      {
        name: 'Senha',
        input: {
          name: 'password',
          type: 'password',
        },
      },
    ]

    const state = {
      email: '',
      password: '',
      errors: []
    }

    return (
      <section className='form-section'>
        <Form
          title="Login"
          state={state}
          inputs={inputs}
          submitHandler={this.submit}
          errors={this.state.errors}
          action="Entrar"
          question="Não possui conta?"
          link="/register"
          answer="Clique Aqui!"
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setProfiles,
    setHeader,
    setHeaderLandingLoginPage
  }

  return bindActionCreators(actions, dispatch)
}

export default connect(null, mapDispatchToProps)(Login)
