import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import gql from 'graphql-tag'

import { client } from '..'
import { setHeader } from '../actions/setHeader'
import { newClassroom } from '../actions/classroomList'
import Form from '../components/Form'

class ClassroomCreate extends Component {
  constructor(props) {
    super(props)

    this.submit = this.submit.bind(this)
  }

  componentDidMount() {
    this.props.setHeader(
      this.props.activeSchool.name,
      () => this.props.history.goBack()
    )
  }

  async submit(args) {
    this.props.newClassroom(args, this.props.activeSchool.id)
    this.props.history.push('/classroom_list')
  }

  render() {
    const inputs = [
      {
        name: 'Ano',
        input: {
          name: 'year',
          type: 'text',
        },
      },
      {
        name: 'Complemento',
        input: {
          name: 'complement',
          type: 'text',
        },
      },
      {
        name: 'Período',
        input: {
          name: 'period',
          type: 'text',
        },
      },
    ]

    const state = {
      year: '',
      complement: '',
      period: '',
    }

    return (
      <section className="form-section">
        <Form
          title="Criar Classe"
          state={state}
          inputs={inputs}
          submitHandler={this.submit}
          action="Criar"
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    newClassroom,
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => ({
  activeSchool: state.activeSchool,
})

export default connect(mapStateToProps, mapDispatchToProps)(ClassroomCreate)
