import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import gql from 'graphql-tag'

import { client } from '..'
import Block from '../components/Block';
import TeamList from '../components/TeamComponents/TeamList'

function isInside(obj, arr) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === obj) return true
  }
  return false
}

function taskDeadlineValidation(task) {
  if (task.deadline == null) return { ...task }
  return { ...task, deadline: new Date(task.deadline.slice(0, -1)) }
}

class ProjectView extends Component {
  constructor(props) {
    super(props)

    this.noCardRender = ['id', 'name', '__typename']

    this.team = null
    this.tasks = null
    this.projectInfo = null

    this.projectAtr = ['Descrição', 'Justificativa', 'Metodologia', 'Marketing', 'Produto', 'Resultado', 'Objetivo']

    this.state = {
      projectInfo: [
        {
          name: 'Descrição',
          question: 'Como você decreveria seu projeto?',
          content: '',
        },
        {
          name: 'Justificativa',
          question: 'Por que voce está fazendo esse projeto?',
          content: '',
        },
        {
          name: 'Metodologia',
          question: 'Como você vai realizar?',
          content: '',
        },
        {
          name: 'Marketing',
          question: 'Como você vai divugar o seu trabalho?',
          content: '',
        },
        {
          name: 'Produto',
          question: 'Qual é o seu produto?',
          content: '',
        },
        {
          name: 'Resultado',
          question: 'Qual foi o resultado final?',
          content: '',
        },
        {
          name: 'Objetivo',
          question: 'Qual é o objetivo do seu projeto?',
          content: '',
        }
      ],
      infoReady: false,
      tasksReady: false,
      teamReady: false,
    }

    this.getProjectData(this.props.activeProject.id)
    this.getTasks(this.props.activeProject.id)
    this.getTeam(this.props.activeProject.id)
  }

  async getProjectData() {
    const getProjectInfo = gql`
      query getProjectInfo($projectId: ID!) {
        getProject(id: $projectId) {
          id
          name
          description
          justification
          methodology
          marketing
          product
          result
          objective
        }
      }
    `
    const response = await client.query({
      query: getProjectInfo,
      variables: {
        projectId: this.props.activeProject.id,
      },
    })

    console.log('Get projectInfo', response)
    this.setState({projectInfo: [
      {
        name: 'Descrição',
        question: 'Como você decreveria seu projeto?',
        content: response.data.getProject.description,
      },
      {
        name: 'Justificativa',
        question: 'Por que voce está fazendo esse projeto?',
        content: response.data.getProject.justification,
      },
      {
        name: 'Metodologia',
        question: 'Como você vai realizar?',
        content: response.data.getProject.methodology,
      },
      {
        name: 'Marketing',
        question: 'Como você vai divugar o seu trabalho?',
        content: response.data.getProject.marketing,
      },
      {
        name: 'Produto',
        question: 'Qual é o seu produto?',
        content: response.data.getProject.product,
      },
      {
        name: 'Resultado',
        question: 'Qual foi o resultado final?',
        content: response.data.getProject.result,
      },
      {
        name: 'Objetivo',
        question: 'Qual é o objetivo do seu projeto?',
        content: response.data.getProject.objective,
      }
    ]})
    console.log('projectInfo state', this.state.projectInfo)
    this.setState({
      infoReady: true,
    })
  }

  async getTasks() {
    const getProjectTasks = gql`
      query getProjectInfo($projectId: ID!) {
        getProject(id: $projectId) {
          id
          tasks {
            id
            title
            description
            deadline
            done
            archived
          }
        }
      }
    `
    const response = await client.query({
      query: getProjectTasks,
      variables: {
        projectId: this.props.activeProject.id,
      },
    })

    this.tasks = response.data.getProject.tasks.map(taskDeadlineValidation)
    this.setState({
      tasksReady: true,
    })
  }

  async getTeam() {
    const getProjectTeam = gql`
      query getProjectInfo($projectId: ID!) {
        getProject(id: $projectId) {
          id
          students {
            id
            email
            name
          }
        }
      }
    `
    const response = await client.query({
      query: getProjectTeam,
      variables: {
        projectId: this.props.activeProject.id,
      },
    })

    this.team = response.data.getProject.students.map(member => {
      return { ...member }
    })
    this.setState({
      teamReady: true,
    })
  }

  getCardList() {
    let cardList = []
    Object.keys(this.projectInfo).forEach(key => {
      if (isInside(key, this.noCardRender) === false)
        cardList.push({ name: key, value: this.projectInfo[key] })
    })
    return cardList
  }

  renderTeam() {
    if (!this.state.teamReady) return ''
    return <TeamList members={this.team} />
  }

  render() {
    if (!this.state.infoReady) return <div> Loading </div>
    return (
      <div className="grid-container">
        <div className="grid-progress">
					<div className="grid-card">
            <div className="block-list">
              {
                this.state.projectInfo.map((atr) => {
                  return (
                    <div className="block-container" key={atr.name}>
                      <Block title={atr.name} subtitle={atr.question} gray={atr.content === '' ? 'gray' : null}></Block>
                    </div>     
                  )
                })
              }
            </div>
          </div>
        </div>
        <div className="grid-team">
          <div className="grid-card">
              <h2>Equipe</h2>
              { this.renderTeam() }
          </div>
        </div>
        <div className="grid-mural">
          <div className="grid-card">Mural</div>
        </div> 
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {}

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    activeProject: state.activeProject,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectView)
