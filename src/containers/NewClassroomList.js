import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FaPencil } from 'react-icons/lib/fa'

import { setHeader } from '../actions/setHeader'
import { fetchClassrooms, selectClassroom } from '../actions/classroomList'
import Block from "../components/Block"

class ClassroomList extends Component {
  constructor(props) {
    super(props)

    this.classroomClick = this.classroomClick.bind(this)
  }

  componentDidMount() {
    if (!this.props.classroomList.length)
      this.props.fetchClassrooms(this.props.activeSchool.id)

    this.props.setHeader(
      'Turmas',
      () => this.props.history.goBack(),
      () => this.props.history.push('/classroom_new')
    )
  }

  classroomClick(obj) {
    this.props.history.push('/project_list')
    this.props.selectClassroom(obj)
  }

  classroomString(classroom) {
    return `${classroom.year}º ${classroom.complement}`
  }

  classroomEdit(obj, ev) {
    ev.stopPropagation()
    this.props.selectClassroom(obj)
    this.props.history.push('/classroom_edit')
  }

  render() {
    return (
      <div className="block-list">
      {
        this.props.classroomList.map((classroom) => {
          return (
            <div key={classroom.id} className="block-container" onClick={ () => this.classroomClick(classroom) } >
              <Block title={ this.classroomString(classroom) } subtitle={classroom.period}/>
              <FaPencil className="block-icon" onClick={ (ev) => this.classroomEdit(classroom, ev)}/>
            </div>
          )
        })
      }
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader,
    fetchClassrooms,
    selectClassroom,
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    classroomList: state.classroomList,
    activeSchool: state.activeSchool,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClassroomList)
