import React, { Component } from 'react'
import gql from 'graphql-tag'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { client } from '..'
import { setHeader } from '../actions/setHeader'
import DropMenu from '../containers/DropMenu'

class ProfileManagement extends Component {
  constructor(props) {
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)

    this.state = {
      email: ''
    }
  }

  componentDidMount() {
    this.props.setHeader(
      "Adicionar Perfil",
      () => this.props.history.goBack()
    )
  }

  async handleSubmit(event) {
    event.preventDefault()

    const connectUserToProfile = gql`
      mutation connectUserToProfileFunc($email: String!, $name: String!) {
        connectUserToProfile(email: $email, name: $name) {
          name
        }
      }
    `

    const response = await client.mutate({
      mutation: connectUserToProfile,
      variables: {
        email: this.state.email,
        name: this.props.selectedProfile,
      },
    })

    this.props.history.push('/menu')
  }

  handleChange(event) {
    this.setState({ [`${event.target.name}`]: event.target.value })
  }

  render() {
    return (
      <section className="form-section">

      <div className="form-container">
        <div className="form-header"> Adicionar Projeto </div>

        <form className="form-content">

          <div className="form-item">
            <p className="form-item-label">Email</p>
            <input
              name="email"
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>

          <div className="form-item">
            <p className="form-item-label">Perfil</p>
            <DropMenu />
          </div>

          <div className="form-button-container">
            <button
              className="btn btn-yellow"
              type="submit"
              onClick={this.handleSubmit}
            >
              Adicionar
            </button>
          </div>
        </form>
      </div>

      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = (state) => {
  return {
    selectedProfile: state.selectedProfile
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileManagement)
