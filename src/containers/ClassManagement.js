import React, { Component } from 'react'
import gql from 'graphql-tag'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { client } from '..'
import { setHeader } from '../actions/setHeader'

class ClassManagement extends Component {
  constructor(props) {
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)

    this.state = {
      email: ''
    }
  }

  componentDidMount() {
    const { year, complement, period } = this.props.activeClass
    const title = `${year}º ${complement} ${period}`

    this.props.setHeader(
      title,
      () => this.props.history.goBack()
    )
  }

  async handleSubmit(event) {
    event.preventDefault()

    const connectUserToClassroom = gql`
      mutation connectUserToClassroomFunc($email: String!, $id: ID!) {
        connectUserToClassroom(email: $email, id: $id) {
          id
        }
      }
    `

    const response = await client.mutate({
      mutation: connectUserToClassroom,
      variables: {
        email: this.state.email,
        id: this.props.activeClass.id
      },
    })

    this.props.history.push('/menu')
  }

  handleChange(event) {
    this.setState({ [`${event.target.name}`]: event.target.value })
  }

  render() {
    return (
      <section className="project_newform-section">

      <div className="form-container">
        <div className="form-header"> Associar aluno a turma </div>

        <form className="form-content">

          <div className="form-item">
            <p className="form-item-label">Email</p>
            <input
              name="email"
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>

          <div className="form-button-container">
            <button
              className="btn btn-yellow"
              type="submit"
              onClick={this.handleSubmit}
            >
              Associar
            </button>
          </div>
        </form>
      </div>

      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => ({
  activeClass: state.activeClass
})

export default connect(mapStateToProps, mapDispatchToProps)(ClassManagement)
