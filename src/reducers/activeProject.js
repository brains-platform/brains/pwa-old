import { SELECT_PROJECT, CLEAN_PROJECT } from '../actions/projectList'

export default (state = null, action) => {
  switch (action.type) {
    case SELECT_PROJECT:
      return action.project

    case CLEAN_PROJECT:
      return action.payload

    default:
      return state
  }
}
