import { SELECT_PROFILE } from '../actions/profileManagement'

export default (state = [], action) => {
  switch(action.type) {
    case SELECT_PROFILE:
      return action.profile

    default:
      return state
  }
}
