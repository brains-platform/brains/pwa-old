import { FETCH_PROJECTS_RESPONSIBLE } from '../actions/projectList'

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_PROJECTS_RESPONSIBLE:
      return action.projectList

    default:
      return state
  }
}
