import { SET_MENU } from '../actions/openMenu'

export default (state = false, action) => {
  switch(action.type) {
    case SET_MENU:
      return action.menuState

    default:
      return state
  }
}
