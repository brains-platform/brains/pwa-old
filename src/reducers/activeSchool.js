import { SELECT_SCHOOL, CLEAN_SCHOOL } from '../actions/schoolList'

export default (state = null, action) => {
  switch (action.type) {
    case SELECT_SCHOOL:
      return action.school

    case CLEAN_SCHOOL:
      return action.payload

    default:
      return state
  }
}
