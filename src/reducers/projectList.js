import { FETCH_PROJECTS, ADD_PROJECT, UPDATE_PROJECT, DELETE_PROJECT } from '../actions/projectList'

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_PROJECTS:
      return action.projectList
    
    case ADD_PROJECT:
      return [...state, action.payload]

    case UPDATE_PROJECT:
      return state.map(project => {
        if (project.id === action.payload.id) {
          return action.payload
        }
        return project
      })

    case DELETE_PROJECT:
      let newState = []
      state.forEach(project => {
        if (project.id !== action.payload.id) {
          newState.push(project)
        }
      })
      return newState

    default:
      return state
  }
}
